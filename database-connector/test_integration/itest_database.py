"""Test module that tests the connection with the database"""

import unittest
import time
from os import environ
from src.database_connector import database

class TestDatabase(unittest.TestCase):
    """Test class that tests the connection with the database"""

    def create_database_connection(self):
        """Create a database connection using the environment variables in .env"""

        return database.Database(
            'db',
            environ['MYSQL_USER'],
            environ['MYSQL_PASSWORD'],
            environ['MYSQL_DATABASE']
            )

    def test_invalid_queries(self):
        """Test whether numbers do no crash the database service"""

        database = self.create_database_connection()

        value_error = 35
        self.assertEqual(database.query(value_error), 'QUERY INVALID')

        value_error = None
        self.assertEqual(database.query(value_error), 'QUERY INVALID')

        value_error = 2423.98
        self.assertEqual(database.query(value_error), 'QUERY INVALID')

        value_error = 'BIEMMMM bong FROM bang'
        self.assertEqual(database.query(value_error), 'QUERY INVALID')

        value_error = '234.32'
        self.assertEqual(database.query(value_error), 'QUERY INVALID')

    def test_basic_queries(self):
        """Test whether the service can insert data into the Users table"""

        database = self.create_database_connection()

        create_table = """CREATE TABLE Demo_Users (
                          UserID  		    INT     	    NOT NULL AUTO_INCREMENT,
                          Username  		VARCHAR(20)     NOT NULL,
                          User_Password  	VARCHAR(20) 	NOT NULL,
                          Email		        VARCHAR(40)	    NOT NULL,
                          Salt		        CHAR(8) 	    NOT NULL,
                          PRIMARY KEY(UserID)
                          );|()"""

        def user_query(username, password, email, salt):
            return f"""INSERT INTO Demo_Users
                       (Username, User_Password, Email, Salt)
                       VALUES (%s, %s, %s, %s);|('{username}', '{password}', '{email}', '{salt}',)"""

        # Users
        insert0 = user_query('Epic Geodude',
                             'Cookies123',
                             'g.rock@students.uu.nl',
                             'asdfflkj')
        insert1 = user_query('Hardcore Onyx',
                             'Stone_brick153',
                             'o.rock@students.uu.nl',
                             'asdasdkj')
        insert2 = user_query('Brock',
                             'Epic_gymmaster_121',
                             'brock.the.rock@uu.nl',
                             'aoipqwer')

        # run tests
        self.assertEqual(database.query(create_table), 0)
        self.assertEqual(database.query(insert0), 0)
        self.assertEqual(database.query(insert1), 0)
        self.assertEqual(database.query(insert2), 0)

        #Test whether the service can retrieve Users data from database
        select = """SELECT Username FROM Demo_Users|()"""
        result = str(database.query(select))
        self.assertTrue('Hardcore Onyx' in result and 'Brock' in result and 'Epic Geodude' in result)

        #Test whether the service can change user entries in the database
        update0 = """UPDATE Demo_Users SET Username = %s
                     WHERE Username = %s;|('Dwayne the Brock', 'Hardcore Onyx')"""
        select0 = """SELECT Username FROM Demo_Users
                     WHERE Username = %s;|('Dwayne the Brock',)"""

        # run tests
        self.assertEqual(database.query(update0), 0)
        self.assertEqual(database.query(select0), (('Dwayne the Brock',),))

    def test_connection_timeout(self):
        """Test whether the connection automatically gets reset after a timeout"""

        database = self.create_database_connection()

        #create table
        create_table = """CREATE TABLE Demo_Purchase (
                          ProductID		    CHAR(36)  	    NOT NULL,
                          Name       		VARCHAR(40)     NOT NULL,
                          PRIMARY KEY(ProductID)
                          );|()"""

        self.assertEqual(database.query(create_table), 0)

        def demo_purchase_query(product_id, name):
            return f"""INSERT INTO Demo_Purchase
                       (ProductID, Name)
                       VALUES(%s, %s)|('{product_id}', '{name}')"""

        #insert data into Demo_Purchase table
        name0 = "Chicken wings"
        name1 = "This is a product"
        name2 = "The geodudes are epic"
        insert0 = demo_purchase_query(0, name0)
        insert1 = demo_purchase_query(1, name1)
        insert2 = demo_purchase_query(2, name2)

        self.assertEqual(database.query(insert0), 0)
        self.assertEqual(database.query(insert1), 0)
        self.assertEqual(database.query(insert2), 0)

        # 'wake up' the connection with a query
        query = "SELECT Name FROM Demo_Purchase WHERE ProductID=%s|('1',)"
        result = database.query(query)
        self.assertEqual(result, ((name1,),))

        #Simluate a timeout by closing the connection
        time.sleep(10)

        # Check if we reconnected and got results after a regular query
        query = "SELECT Name FROM Demo_Purchase WHERE ProductID=%s|('2',)"
        result = database.query(query)
        self.assertEqual(result, ((name2,),))

if __name__ == '__main__':
    unittest.main()

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
