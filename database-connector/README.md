# Utrecht Companion To The Earth - database-connector
<INSERT SUMMARY>

## API
### <SELECT query>
<Insert summary> 
    Performs the given select query upon the server-side database.  
    Triggered by sending a query starting with 'SELECT'.  
    If succesfull, returns the results to middleware-db, which in turn  sends the results back to the original sender, with the same correlation_id.  
    If not succesfull, returns an error code with the corresponding error message.

###### Example
<Insert example> 
    self.publish('middleware-db', 'SELECT * FROM Locations WHERE Location_ID = -1', 'foo', '17')  
    (Query gets sent to the database)  
    (Here the return value would be a message to 'foo' with correlation_id = '17' and with body = '()' as Locations doesn't contain any negative ID's.)

###### Example
<Insert example> 
    self.publish('middleware-db', 'SELECT * FROM NotATable', 'foo', '17')
    (Query gets sent to the database)  
    (Here the return value would be a message to 'foo' with correlation_id = '17' and with body = 'ERROR 1146: Table 'NotATable' doesn't exist'.)

## API
### <Alter, Insert, Update, Delete and Create queriy>
<Insert summary> 
    Performs a Alter, Insert, Update, Delete or Create query on the database.  
    Queries starting with 'ALTER', 'INSERT', 'UPDATE', 'DELETE' or 'CREATE' trigger this call.  
    Returns a 0 value in case of succes, or an error code with error message in case of failure.    

###### Example
<Insert example> 
    self.publish('middleware-db', 
    'INSERT INTO Classes (ClassID) VALUES ("3")',
     'foo', '22')  
    (Query gets sent to the database)  
    (Here the return value would be a message to 'foo' with correlation_id = '22' and with body = '0' to confirm the insert has been performed.)

###### Example
<Insert example> 
    self.publish('middleware-db', 
    'CREATE TABLE Faculties (
    FacultyID 		CHAR(36)  	    NOT NULL,
    PRIMARY KEY(FacultyID));',
    'foo', '22')  
    (Query gets sent to the database)  
    (Here the return value would be a message to 'foo' with correlation_id = '22' and with body = '1050: Table 'Faculties' already exists'.)

## API
### <Misc queries>
<Insert summary> 
    Only above mentioned query formats are accepted by the database.  
    Sending any other mysql statement gives a return value of 'QUERY INVALID' with correlation_id corresponding to the sender's.
    
###### Example
<Insert example>
    self.publish('middleware-db', 
    'DROP TABLE Pins',
    'foo', '3')  
    (Query gets sent to the database)  
    (Here the return value would be a message to 'foo' with correlation_id = '3' and with body = 'INVALID QUERY'.)

## libraries
* MySQLdb: <https://mysqlclient.readthedocs.io/>
