import setuptools

setuptools.setup(
    name="database-connector",
    version="1.5.2",
    description="AMQP to database bridge",
    packages=setuptools.find_packages(where="src"),
    package_dir={"":"src"},
    install_requires=[
        "mysqlclient~=2.0.3"
    ],
    python_requires=">=3.8",
)