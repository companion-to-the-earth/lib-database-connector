"""Database module that manages the database connection and executes queries"""

import time
import ast
from os import environ
import MySQLdb

class Database:
    """Provides an interface for the database connection to improve readablity"""

    __connection = None
    __cursor = None

    host = ''
    username = ''
    password = ''
    database = ''

    def __init__(self, host: str, username: str, password: str, database: str):
        self.host = host
        self.username = username
        self.password = password
        self.database = database

        self.__connect()

    def __connect(self):
        """Attempt to connect to the database for X amounts of seconds. \
           If this process takes too long the function returns a non-zero value: 1.

           Returns a non-zero value if something failed while establishing a connection.
        """

        attempt = 0
        max_attempts = 40
        interval = 1

        print("Attempt to (re)connect")

        while attempt < max_attempts:
                
            try:
                # Establish a TCP connection            
                self.__connection = MySQLdb.connect(host=self.host,
                                                    port=3306,
                                                    user=self.username,
                                                    passwd=self.password,
                                                    db=self.database)
                if self.__connection:
                    self.__cursor = self.__connection.cursor()
                    print(f'Established connection with {self.host}')
                    return True

            except (MySQLdb.OperationalError, MySQLdb.DatabaseError) as exception:
                self.__print_error_message(str(exception))

            #retry after interval seconds
            print("attempt "+str(attempt)+" -> retrying")
            time.sleep(interval)
            attempt += 1

        # after max_attempts tries, we give up, returning a non-zero value
        print(f'Gave up connecting after more than {max_attempts} attempts \
                with intervals of {interval} second(s).')
        return False

    def close_connection(self):
        """Close connection with both the cursor and the database."""

        self.__cursor.close()
        self.__connection.close()

    def __select(self, query: str):
        """select from table"""
        query, params = query.split("|")
        params = ast.literal_eval(params)

        # Execute the SQL command
        self.__cursor.execute(query, params)
        # Fetch all lines instead of just the first line
        data = self.__cursor.fetchall()
        self.__connection.commit()
        return data

    def __print_error_message(self, exception):
        print(exception)
        code, message = ast.literal_eval(exception)
        return f'ERROR {code} {message}'

    def __alter(self, query: str):
        """change, add or delete table entry"""
        query, params = query.split("|")
        params = ast.literal_eval(params)
            # Execute the SQL command
        self.__cursor.execute(query, params)
        self.__connection.commit()
        return 0

    def query(self, query: str, attempt=0):
        """
           Send query to database.

           In case the database connection is lost before or during this function call,
           connect, increment attempt and call this function again.

           This behaviour renders this function finite recursive.

           :param str query: An SQL query in string format
           :param int attempt: An integer that tracks the query function depth.
        """

        #return 'NO_CONNECTION' if we have tried too often
        max_depth=2

        if attempt > max_depth:
            return 'NO_CONNECTION'

        if type(query) == bytes:
            query = bytes(query).decode("utf-8")
        query = str(query)

        # Pick whether the query is a select, alter or invalid query by judging the first word
        try:
            return self.__query_filter(query)
      
        except MySQLdb._exceptions.ProgrammingError as exception:     
            #check whether we have to reconnect to the database
            if str(exception).__contains__('cursor closed'):
                self.__connect()

        except Exception as exception:
            #check whether we have to reconnect to the database
            code, message = ast.literal_eval(str(exception))

            if code == 2006 or code == 2013:
                self.__connect()
                return self.query(query, attempt + 1)

            #return error if it could not be solved by the library itself
            return self.__print_error_message(str(exception))

    def __query_filter(self, query: str):
        """Calls __select or __alter depending on the query keyword"""

        keyword = query.split(' ')[0]
        if keyword == 'SELECT':
            return self.__select(query)
        if keyword in ('ALTER', 'INSERT', 'UPDATE', 'DELETE', 'CREATE'):
            return self.__alter(query)
        return 'QUERY INVALID'

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)

